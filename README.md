# Inw-Utility

__Table of contents__

- [INW-UTILITY](#path-logger)
  - [Installation](#installation)
  - [Usage](#usage)

## Installation

`inw-utility` can be installed on Linux, Mac OS or Windows without any issues.

Install via NPM

```
npm install inw-utility --save
```

## Usage
1. Import

```javascript
import Utility from 'inw-utility';
```

2. Usage

```javascript
const util = new Utility();
util.isNull(null)// => true
```

