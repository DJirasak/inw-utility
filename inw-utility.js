class Utility {

    formatDate(strDateFull, mark = "-"){
        let arrMonth = [ "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];
        let strDate = strDateFull.split(" ");
        let arrDate = strDate[0].split(mark); // YYYY-MM-DD
        let nYear = String(Number(arrDate[0]) + 543);
        let nMonth = arrMonth[Number(arrDate[1]) - 1];
        let nDay = String(Number(arrDate[2]));
        return nDay + " " + nMonth + " " + nYear;
    }
    
    formatPrice(numInp, digit = 2){
        const options = { 
            minimumFractionDigits: digit,
            maximumFractionDigits: digit 
        };
        const formatted = Number(numInp).toLocaleString('en', options);
        return formatted;
    }
    
    reformName(name){
        let newName = ""
        if((name.charAt(0) === "ด" && (name.charAt(1) === "ช" || name.charAt(1) === "ญ")) || (name.charAt(0) === "น" && name.charAt(1) === "ส")){
            newName = name.substring(2);
            // newName = name.replace("ดช","").replace("ดญ","").replace("นส","")
        }else if((name.charAt(0) === "น" && name.charAt(1) === "า") && name.charAt(2) === "ย"){
            newName = name.substring(3);
        }
        else if(((name.charAt(0) === "น" && name.charAt(1) === "า") && (name.charAt(2) === "ง" && name.charAt(3) === "ส"))&&(name.charAt(4) === "า" && name.charAt(5) === "ว")){
            newName = name.substring(6);
        }
        else if((name.charAt(0) === "น" && name.charAt(1) === "า") && name.charAt(2) === "ง"){
            newName = name.substring(3);
        }
        else{
            newName = name.replace("ด.ช.","").replace("ด.ช","").replace("ดช.","").replace("ด.ญ.","").replace("ด.ญ","").replace("ดญ.","").replace("เด็กชาย","").replace("เด็กหญิง","").replace("เด็กชาย.","").replace("เด็กหญิง.","").replace("นาย.","").replace("นาง.","").replace("นางสาว.","").replace("น.ส.","").replace("นส.","").replace("น.ส","")
        }
        return newName;
    }
    
    reformTel(tel){
        return tel.replace(/[^0-9.]/g, '');
    }
    
    payloadEncode(source){
        return JSON.parse(`{"payload": "${btoa(unescape(encodeURIComponent(JSON.stringify(source))))}"}`);
    }
    
    payloadDecode(payload){
        return JSON.parse(this.base64Decode(payload));
    }
    
    base64Encode(raw_data){
        return btoa(unescape(encodeURIComponent(raw_data)));
    }
    
    base64Decode(encode_data){
        return decodeURIComponent(escape(atob(encode_data)));
    }

    isCitizenId(p_iPID){
        let total = 0;
        let iPID = p_iPID.replace(/[^A-Z0-9]+/ig, "");
        let Validchk = iPID.substr(12, 1);
        let j = 0;
        let pidcut;
        for (var n = 0; n < 12; n++) {
            pidcut = parseInt(iPID.substr(j, 1));
            total = (total + ((pidcut) * (13 - n)));
            j++;
        }
        let chk = 11 - (total % 11);
        chk = (chk==10) ? 0 : (chk==11) ? 1 : chk;
        return (chk == Validchk)?true:false;
    }

    timeSince(dateInp){
		dateInp = dateInp.replace(/-/g,"/");
        let date = Date.parse(dateInp);
        let seconds = Math.floor((new Date() - date) / 1000);
        let interval = seconds / 31536000;
        interval = seconds / 60;
        interval = seconds / 86400;
        if (interval >= 2) {
            return fotmatDate(dateInp, "/");
        } else if (interval > 1) {
            return "เมื่อวาน";
        }
        interval = seconds / 3600;
        if (interval > 1) {
            return Math.floor(interval) + " ชั่วโมงที่แล้ว";
        }
        interval = seconds / 60;
        if (interval > 1) {
            return Math.floor(interval) + " นาทีที่ผ่านมา";
        }
        return "เมื่อสักครู่";
    }

    getParams(){
        if(arguments.length == 0){
            const urlSearchParams = new URLSearchParams(window.location.search);
            const params = Object.fromEntries(urlSearchParams.entries());
            return params;
        }else if(arguments.length == 1){
            let url = window.location.href;
            let name = arguments[0];
            name = name.replace(/[\[\]]/g, '\\$&');
            let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }
    }
    
    isNull(value){
        return (value === null || value === undefined) ? true : false;
    }
    
    isEmpty(value){
        return (value === "") ? true : false;
    }
}

export default Utility;